const languages = ["en", "fr", "es", "ca", "eo", "eu", "nb", "zh"]
const validPrefixes = ["main", "action", "question", "status", "title", "error", "tooltip"]

module.exports = {
    languages,
    validPrefixes
}
