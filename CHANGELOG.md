# 0.8.7

- Using Flutter 1.22
- Upgraded dependencies

# 0.8.6

- Adding push notification support
- Allowing to choose the UI language
- Allow to persist UI settings

# 0.8.5

- Using native symmetric encryption
- Using HTTP gateway connections instead of Web Sockets
- Improving the refresh of UI data
